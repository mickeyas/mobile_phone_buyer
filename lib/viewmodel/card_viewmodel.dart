import 'package:flutter/cupertino.dart';
import 'package:mobile_phone_buyer/model/mobile.dart';
import 'package:mobile_phone_buyer/model/mobile_image.dart';
import 'package:mobile_phone_buyer/repository/mobile_image_repository.dart';
import 'package:mobile_phone_buyer/repository/mobile_repository.dart';

class CardViewModel extends ChangeNotifier {
  BuildContext context;
  bool _disposed = false;
  MobileRepository _mobileRepository = MobileRepository();
  MobileImageRepository _mobileImageRepository = MobileImageRepository();

  List<Mobile> mobiles = [];
  List<MobileImage> images = [];

  @override
  void dispose() {
    _disposed = true;
    super.dispose();
  }

  @override
  void notifyListeners() {
    if (!_disposed) {
      super.notifyListeners();
    }
  }

  Future<void> fetchMobiles() async {
    try {
      mobiles = await _mobileRepository.mobiles;
      notifyListeners();
    } on Exception catch (e) {
      print(e);
    }
  }

  Future<void> fetchMobileImages(String id) async {
    images = [];
    try {
      images = await _mobileImageRepository.mobileImages(id);
      notifyListeners();
    } on Exception catch (e) {
      print(e);
    }
  }

  List<Mobile> get favouriteMobiles {
    return mobiles.where((mobile) => mobile.isFavourite).toList();
  }

  void sortMobilesByPriceLowToHigh() {
    mobiles.sort((a, b) => a.price.compareTo(b.price));
    notifyListeners();
  }

  void sortMobilesByPriceHighToLow() {
    mobiles.sort((a, b) => b.price.compareTo(a.price));
    notifyListeners();
  }

  void sortMobilesByRating() {
    mobiles.sort((a, b) => a.rating.compareTo(b.rating));
    notifyListeners();
  }

  void toggleFavourite(int mobileIndex) {
    mobiles[mobileIndex].isFavourite = !mobiles[mobileIndex].isFavourite;
    notifyListeners();
  }
}
