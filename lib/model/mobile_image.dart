class MobileImage {
  int mobileId;
  int id;
  String url;

  MobileImage({this.mobileId, this.id, this.url});

  MobileImage.fromJson(Map<dynamic, dynamic> json) {
    mobileId = json['mobile_id'];
    id = json['id'];
    url = json['url'] ?? "";
  }

  Map<dynamic, dynamic> toJson() {
    final Map<dynamic, dynamic> data = new Map<dynamic, dynamic>();
    data['mobile_id'] = this.mobileId;
    data['id'] = this.id;
    data['url'] = this.url;
    return data;
  }

  String get httpsUrl {
    RegExp regExp = new RegExp(
      r"^www.",
      caseSensitive: false,
      multiLine: false,
    );

    return regExp.hasMatch(url) ? "https://${url}" : url;
  }
}
