class Mobile {
  double rating;
  String name;
  String brand;
  num price;
  String thumbImageURL;
  String description;
  int id;
  bool isFavourite = false;

  Mobile(
      {this.rating,
      this.name,
      this.brand,
      this.price,
      this.thumbImageURL,
      this.description,
      this.id});

  Mobile.fromJson(Map<dynamic, dynamic> json) {
    rating = json['rating'];
    name = json['name'] ?? "";
    brand = json['brand'] ?? "";
    price = json['price'];
    thumbImageURL = json['thumbImageURL'] ?? "";
    description = json['description'] ?? "";
    id = json['id'];
  }

  Map<dynamic, dynamic> toJson() {
    final Map<dynamic, dynamic> data = new Map<dynamic, dynamic>();
    data['rating'] = this.rating;
    data['name'] = this.name;
    data['brand'] = this.brand;
    data['price'] = this.price;
    data['thumbImageURL'] = this.thumbImageURL;
    data['description'] = this.description;
    data['id'] = this.id;
    return data;
  }
}
