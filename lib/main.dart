import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:mobile_phone_buyer/service_locator.dart';
import 'package:mobile_phone_buyer/widget/home_page.dart';

void main() {
  ServiceLocator _serviceLocator = ServiceLocator();
  _serviceLocator.setUp();

  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations(
        [DeviceOrientation.portraitDown, DeviceOrientation.portraitUp]);

    return MaterialApp(routes: {
      '/': (context) => Home(),
    });
  }
}
