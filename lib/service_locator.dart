import 'package:get_it/get_it.dart';
import 'package:mobile_phone_buyer/viewmodel/card_viewmodel.dart';

class ServiceLocator {
  final getIt = GetIt.instance;

  void setUp() {
    getIt.registerSingleton<CardViewModel>(CardViewModel());
  }
}
