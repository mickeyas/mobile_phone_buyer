import 'package:mobile_phone_buyer/http/api_base_helper.dart';
import 'package:mobile_phone_buyer/http/url.dart';
import 'package:mobile_phone_buyer/model/mobile_image.dart';

class MobileImageRepository {
  ApiBaseHelper api = ApiBaseHelper();

  Future<List<MobileImage>> mobileImages(String id) async {
    var jsonResponse;
    jsonResponse = await api.get(
        "${EndPoint.url}${EndPoint.listMobile}/${id}/${EndPoint.listImage}");

    return List<MobileImage>.from(
        jsonResponse.map((image) => MobileImage.fromJson(image)));
  }
}
