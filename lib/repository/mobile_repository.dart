import 'package:mobile_phone_buyer/http/api_base_helper.dart';
import 'package:mobile_phone_buyer/http/url.dart';
import 'package:mobile_phone_buyer/model/mobile.dart';

class MobileRepository {
  ApiBaseHelper api = ApiBaseHelper();

  Future<List<Mobile>> get mobiles async {
    var jsonResponse;
    jsonResponse = await api.get("${EndPoint.url}${EndPoint.listMobile}");

    return List<Mobile>.from(
        jsonResponse.map((mobile) => Mobile.fromJson(mobile)));
  }
}
