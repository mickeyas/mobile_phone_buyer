import 'dart:io';

import 'package:dio/dio.dart';

import 'app_exception.dart';

class ApiBaseHelper {
  Dio dio = Dio();

  Future<dynamic> get(String url) async {
    var responseJson;
    try {
      final response = await dio.get(
        url,
      );
      responseJson = returnResponse(response);
    } on DioError catch (e) {
      await returnResponse(e.response);
    } on SocketException {
      throw FetchDataException('No Internet connection----');
    }

    return responseJson;
  }

  dynamic returnResponse(Response response) async {
    if (response == null) {
      throw FetchDataException('No Internet connection');
    }

    switch (response?.statusCode) {
      case 200:
        // var responseJson = jsonDecode(response.data);
        var responseMap = response.data;
        print(responseMap);
        return responseMap;
      case 400:
        throw BadRequestException(response.data);
      case 401:
      case 403:
        throw UnauthorisedException(response.data);
      case 500:
      default:
        throw FetchDataException(
            'Error occured while Communication with Server with StatusCode : ${response.statusCode}');
    }
  }
}
