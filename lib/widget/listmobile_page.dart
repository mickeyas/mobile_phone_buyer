import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:mobile_phone_buyer/viewmodel/card_viewmodel.dart';
import 'package:mobile_phone_buyer/widget/component/card_mobile.dart';
import 'package:mobile_phone_buyer/widget/show_mobile_detail.dart';
import 'package:stacked/stacked.dart';

class ListMobile extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<CardViewModel>.reactive(
        viewModelBuilder: () => GetIt.instance<CardViewModel>(),
        disposeViewModel: false,
        builder: (context, viewModel, child) {
          return ListView.builder(
              padding: const EdgeInsets.all(8),
              itemCount: viewModel.mobiles.length,
              itemBuilder: (BuildContext context, int index) {
                return InkWell(
                  key: Key("card${viewModel.mobiles[index].id}"),
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) =>
                              ShowMobileDetail(viewModel.mobiles[index])),
                    );
                  },
                  child: CardMobile(
                    viewModel.mobiles[index],
                    onFavouriteTab: () {
                      viewModel.toggleFavourite(index);
                    },
                    showFavouriteIcon: true,
                  ),
                );
              });
        });
  }
}
