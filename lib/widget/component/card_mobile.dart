import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mobile_phone_buyer/model/mobile.dart';

class CardMobile extends StatelessWidget {
  final Mobile mobile;
  final VoidCallback onFavouriteTab;
  final bool showFavouriteIcon;
  CardMobile(this.mobile,
      {@required this.onFavouriteTab, this.showFavouriteIcon = false});
  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      child: Row(
        children: [
          Expanded(
            flex: 2,
            child: Image.network(
              mobile.thumbImageURL,
              fit: BoxFit.cover,
            ),
          ),
          Expanded(
            flex: 4,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  child: Row(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        child: Text(
                          "${mobile.brand} ${mobile.name}",
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                      ),
                      if (showFavouriteIcon)
                        Container(
                          alignment: Alignment.centerRight,
                          child: InkWell(
                              key: Key(mobile.id.toString()),
                              onTap: this.onFavouriteTab,
                              child: Icon(
                                  mobile.isFavourite
                                      ? Icons.star
                                      : Icons.star_border,
                                  color: Colors.lightBlue)),
                        )
                    ],
                  ),
                ),
                Container(
                  child: Text(
                    mobile.description,
                    overflow: TextOverflow.ellipsis,
                    maxLines: 2,
                    softWrap: false,
                  ),
                ),
                Container(
                  child: Row(
                    children: [
                      Text("Price: \$ ${mobile.price}"),
                      Expanded(
                        child: Text(
                          "Rating: \$ ${mobile.rating}",
                          textAlign: TextAlign.end,
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
