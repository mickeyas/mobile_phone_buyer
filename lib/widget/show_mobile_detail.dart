import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:mobile_phone_buyer/model/mobile.dart';
import 'package:mobile_phone_buyer/viewmodel/card_viewmodel.dart';
import 'package:stacked/stacked.dart';

class ShowMobileDetail extends StatelessWidget {
  final Mobile mobile;
  ShowMobileDetail(this.mobile);

  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<CardViewModel>.reactive(
        viewModelBuilder: () => GetIt.instance<CardViewModel>(),
        disposeViewModel: false,
        onModelReady: (viewModel) async {
          GetIt.instance<CardViewModel>()
              .fetchMobileImages(mobile.id.toString());
        },
        builder: (context, viewModel, child) {
          return Scaffold(
            appBar: AppBar(
              title: Text("${mobile.brand} ${mobile.name}"),
            ),
            body: viewModel.images.isEmpty
                ? Center(
                    child: CircularProgressIndicator(),
                  )
                : Container(
                    child: Column(
                      children: [
                        CarouselSlider(
                          options: CarouselOptions(
                            autoPlay: true,
                            aspectRatio: 2.0,
                          ),
                          items: viewModel.images
                              .map((image) => Container(
                                    padding: EdgeInsets.only(top: 30),
                                    margin: EdgeInsets.all(5.0),
                                    child: ClipRRect(
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(5.0)),
                                        child: Stack(
                                          children: <Widget>[
                                            Container(
                                              height: MediaQuery.of(context)
                                                      .size
                                                      .height *
                                                  0.2,
                                              child: Image.network(
                                                  image.httpsUrl,
                                                  fit: BoxFit.cover),
                                            ),
                                          ],
                                        )),
                                  ))
                              .toList(),
                        ),
                        Container(
                          alignment: Alignment.topLeft,
                          padding:
                              EdgeInsets.only(top: 20, bottom: 20, left: 10),
                          child: Text(
                            "${mobile.brand} ${mobile.name}",
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                        ),
                        Container(
                            padding: EdgeInsets.only(left: 10),
                            child: Text("${mobile.description}")),
                      ],
                    ),
                  ),
          );
        });
  }
}
