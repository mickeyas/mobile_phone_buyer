import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:mobile_phone_buyer/viewmodel/card_viewmodel.dart';

class SortDialog {
  showAlertDialog(
    BuildContext context,
  ) {
    showDialog(
        barrierDismissible: false,
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text(
              "Sort",
              textAlign: TextAlign.center,
            ),
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(40.0))),
            content: Wrap(
              alignment: WrapAlignment.center,
              children: <Widget>[
                Column(
                  children: <Widget>[
                    Container(
                      alignment: Alignment.center,
                      height: 50,
                      child: InkWell(
                        key: Key("sort_asc"),
                        onTap: () {
                          GetIt.instance<CardViewModel>()
                              .sortMobilesByPriceLowToHigh();
                          Navigator.pop(context);
                        },
                        child: Text(
                          "Price low to high",
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ),
                    Container(
                      alignment: Alignment.center,
                      height: 50,
                      child: InkWell(
                        key: Key("sort_desc"),
                        onTap: () {
                          GetIt.instance<CardViewModel>()
                              .sortMobilesByPriceHighToLow();
                          Navigator.pop(context);
                        },
                        child: Text(
                          "Price high to low",
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ),
                    Container(
                      alignment: Alignment.center,
                      height: 50,
                      child: InkWell(
                        key: Key("sort_rating"),
                        onTap: () {
                          GetIt.instance<CardViewModel>().sortMobilesByRating();
                          Navigator.pop(context);
                        },
                        child: Text(
                          "Rating",
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ),
                    Container(
                      alignment: Alignment.center,
                      height: 50,
                      child: InkWell(
                        key: Key("cancel"),
                        onTap: () {
                          Navigator.pop(context);
                        },
                        child: Text(
                          "Cancel",
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          );
        });
  }
}
