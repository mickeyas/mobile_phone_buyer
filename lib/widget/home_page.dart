import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:mobile_phone_buyer/viewmodel/card_viewmodel.dart';
import 'package:mobile_phone_buyer/widget/alertdialog/sort_dialog.dart';
import 'package:mobile_phone_buyer/widget/favorite_mobile_page.dart';
import 'package:mobile_phone_buyer/widget/listmobile_page.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  @override
  void initState() {
    // TODO: implement initState

    super.initState();
    GetIt.instance<CardViewModel>().fetchMobiles();
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: MaterialApp(
        home: Scaffold(
          appBar: AppBar(
            bottom: TabBar(
              onTap: (index) {},
              tabs: [
                Tab(key: Key("all"), text: "All"),
                Tab(key: Key("favourite"), text: "Favourite"),
              ],
            ),
            title: Container(
              alignment: Alignment.centerRight,
              child: InkWell(
                key: Key("sort"),
                onTap: () {
                  SortDialog().showAlertDialog(this.context);
                },
                child: Text("Sort"),
              ),
            ),
          ),
          body: TabBarView(
            children: [
              ListMobile(),
              FavoriteMobile(),
            ],
          ),
        ),
      ),
    );
  }
}
