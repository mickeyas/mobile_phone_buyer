import 'package:flutter/cupertino.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:integration_test/integration_test.dart';
import 'package:mobile_phone_buyer/main.dart' as app;
import 'package:mobile_phone_buyer/viewmodel/card_viewmodel.dart';

void main() {
  group("Mobile phone buyer app test ", () {
    IntegrationTestWidgetsFlutterBinding.ensureInitialized();
    testWidgets("full app test", (tester) async {
      app.main();
      await tester.pumpAndSettle();
      CardViewModel cardViewModel = CardViewModel();
      await cardViewModel.fetchMobiles();
      await tester.pumpAndSettle();
      final cardMobileInkwell =
          find.byKey(Key("card${cardViewModel.mobiles[0].id}"));
      final favouriteInkwell =
          find.byKey(Key(cardViewModel.mobiles[0].id.toString()));
      final sortInkwell = find.byKey(Key("sort"));
      final sortAscInkwell = find.byKey(Key("sort_asc"));
      final sortDescInkwell = find.byKey(Key("sort_desc"));
      final sortRating = find.byKey(Key("sort_rating"));
      final cancel = find.byKey(Key("cancel"));
      final favouriteTab = find.byKey(Key("favourite"));
      final allTab = find.byKey(Key("all"));
      await tester.tap(favouriteInkwell);
      await tester.pumpAndSettle();
      await tester.tap(favouriteTab);
      await tester.pumpAndSettle();
      await tester.tap(allTab);
      await tester.pumpAndSettle();
      await tester.tap(sortInkwell);
      await tester.pumpAndSettle();
      await tester.tap(sortAscInkwell);
      await tester.pumpAndSettle();
      await tester.tap(sortInkwell);
      await tester.pumpAndSettle();
      await tester.tap(sortDescInkwell);
      await tester.pumpAndSettle();
      await tester.tap(sortInkwell);
      await tester.pumpAndSettle();
      await tester.tap(sortRating);
      await tester.pumpAndSettle();
      await tester.tap(sortInkwell);
      await tester.pumpAndSettle();
      await tester.tap(cancel);
      await tester.pumpAndSettle();
      await tester.tap(cardMobileInkwell);
      await tester.pumpAndSettle();
    });
  });
}
