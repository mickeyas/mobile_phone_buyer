import 'package:mobile_phone_buyer/model/mobile_image.dart';
import 'package:test/test.dart';

void main() {
  group('Mobile image model', () {
    test('should parse json to mobile image model correctly', () async {
      dynamic json = {
        "url":
            "https://www.91-img.com/gallery_images_uploads/d/d/ddd018034fd6b03b55683e5dca3c0b256621cfdc.jpg",
        "id": 4,
        "mobile_id": 4
      };

      MobileImage mobileImage = MobileImage.fromJson(json);

      expect(mobileImage.url, json['url']);
      expect(mobileImage.id, json['id']);
      expect(mobileImage.mobileId, json['mobile_id']);
    });

    test('should parse empty json to mobile image model', () async {
      dynamic json = {};

      MobileImage mobileImage = MobileImage.fromJson(json);

      expect(mobileImage.url, "");
      expect(mobileImage.id, null);
      expect(mobileImage.mobileId, null);
    });

    group('httpsUrl ', () {
      test('should return https if url is starting with www.', () async {
        MobileImage mobileImage = MobileImage(url: "www.test.com");

        expect(mobileImage.httpsUrl, "https://www.test.com");
      });

      test('should return same url if url is starting with https://', () async {
        MobileImage mobileImage = MobileImage(url: "https://www.test.com");

        expect(mobileImage.httpsUrl, mobileImage.url);
      });
    });
  });
}
