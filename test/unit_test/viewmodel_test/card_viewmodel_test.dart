import 'package:mobile_phone_buyer/model/mobile.dart';
import 'package:mobile_phone_buyer/viewmodel/card_viewmodel.dart';
import 'package:test/test.dart';

void main() {
  group('Card viewmodel', () {
    group('favouriteMobiles', () {
      test('should get empty list', () async {
        CardViewModel cardViewModel = CardViewModel();
        cardViewModel.mobiles = <Mobile>[
          Mobile(
              rating: 10,
              brand: "test",
              description: "for test card viewmodel",
              id: 1,
              name: "test",
              price: 20.0,
              thumbImageURL: "www.test.com"),
          Mobile(
              rating: 30,
              brand: "test2",
              description: "for test card viewmodel2",
              id: 2,
              name: "test2",
              price: 30.0,
              thumbImageURL: "www.test2.com")
        ];

        expect(cardViewModel.favouriteMobiles.length, 0);
      });

      test('should get favourite list', () async {
        CardViewModel cardViewModel = CardViewModel();
        cardViewModel.mobiles = <Mobile>[
          Mobile(
              rating: 10,
              brand: "test",
              description: "for test card viewmodel",
              id: 1,
              name: "test",
              price: 20.0,
              thumbImageURL: "www.test.com"),
          Mobile(
              rating: 30,
              brand: "test2",
              description: "for test card viewmodel2",
              id: 2,
              name: "test2",
              price: 30.0,
              thumbImageURL: "www.test2.com")
        ];
        cardViewModel.mobiles[0].isFavourite = true;
        expect(cardViewModel.favouriteMobiles.length, 1);
        expect(cardViewModel.favouriteMobiles[0].rating, 10);
        expect(cardViewModel.favouriteMobiles[0].description,
            "for test card viewmodel");
        expect(cardViewModel.favouriteMobiles[0].id, 1);
        expect(cardViewModel.favouriteMobiles[0].name, "test");
        expect(cardViewModel.favouriteMobiles[0].price, 20.0);
        expect(cardViewModel.favouriteMobiles[0].thumbImageURL, "www.test.com");
      });
    });

    group('fetch model', () {
      test('should get mobiles list', () async {
        CardViewModel cardViewModel = CardViewModel();
        await cardViewModel.fetchMobiles();

        expect(cardViewModel.mobiles.length > 0, true);
      });

      test('should get images list', () async {
        CardViewModel cardViewModel = CardViewModel();
        cardViewModel.mobiles = <Mobile>[
          Mobile(
              rating: 10,
              brand: "test",
              description: "for test card viewmodel",
              id: 1,
              name: "test",
              price: 20.0,
              thumbImageURL: "www.test.com"),
          Mobile(
              rating: 30,
              brand: "test2",
              description: "for test card viewmodel2",
              id: 2,
              name: "test2",
              price: 30.0,
              thumbImageURL: "www.test2.com")
        ];
        await cardViewModel
            .fetchMobileImages(cardViewModel.mobiles[0].id.toString());
        expect(cardViewModel.images.length > 0, true);
      });
    });

    group('toggleFavourite', () {
      test('should able to toggle twice correctly', () async {
        CardViewModel cardViewModel = CardViewModel();
        cardViewModel.mobiles = <Mobile>[
          Mobile(
              rating: 10,
              brand: "test",
              description: "for test card viewmodel",
              id: 1,
              name: "test",
              price: 20.0,
              thumbImageURL: "www.test.com"),
          Mobile(
              rating: 30,
              brand: "test2",
              description: "for test card viewmodel2",
              id: 2,
              name: "test2",
              price: 30.0,
              thumbImageURL: "www.test2.com")
        ];
        cardViewModel.toggleFavourite(0);
        expect(cardViewModel.mobiles[0].isFavourite, true);
        cardViewModel.toggleFavourite(0);
        expect(cardViewModel.mobiles[0].isFavourite, false);
      });
    });

    group('sortMobiles', () {
      test('Should get sort mobiles by price low to high', () async {
        CardViewModel cardViewModel = CardViewModel();
        cardViewModel.mobiles = <Mobile>[
          Mobile(
              rating: 10,
              brand: "test",
              description: "for test card viewmodel",
              id: 1,
              name: "test",
              price: 20.0,
              thumbImageURL: "www.test.com"),
          Mobile(
              rating: 30,
              brand: "test2",
              description: "for test card viewmodel2",
              id: 2,
              name: "test2",
              price: 30.0,
              thumbImageURL: "www.test2.com")
        ];
        cardViewModel.sortMobilesByPriceLowToHigh();
        expect(cardViewModel.mobiles[0].price < cardViewModel.mobiles[1].price,
            true);
      });

      test('Should get sort mobiles by price high to low', () async {
        CardViewModel cardViewModel = CardViewModel();
        cardViewModel.mobiles = <Mobile>[
          Mobile(
              rating: 10,
              brand: "test",
              description: "for test card viewmodel",
              id: 1,
              name: "test",
              price: 20.0,
              thumbImageURL: "www.test.com"),
          Mobile(
              rating: 30,
              brand: "test2",
              description: "for test card viewmodel2",
              id: 2,
              name: "test2",
              price: 30.0,
              thumbImageURL: "www.test2.com")
        ];
        cardViewModel.sortMobilesByPriceHighToLow();
        expect(cardViewModel.mobiles[0].price > cardViewModel.mobiles[1].price,
            true);
      });

      test('Should get sort mobiles by rating', () async {
        CardViewModel cardViewModel = CardViewModel();
        cardViewModel.mobiles = <Mobile>[
          Mobile(
              rating: 10,
              brand: "test",
              description: "for test card viewmodel",
              id: 1,
              name: "test",
              price: 20.0,
              thumbImageURL: "www.test.com"),
          Mobile(
              rating: 30,
              brand: "test2",
              description: "for test card viewmodel2",
              id: 2,
              name: "test2",
              price: 30.0,
              thumbImageURL: "www.test2.com")
        ];
        cardViewModel.sortMobilesByRating();
        expect(
            cardViewModel.mobiles[0].rating < cardViewModel.mobiles[1].rating,
            true);
      });
    });
  });
}
